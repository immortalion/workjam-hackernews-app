import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import Comments from "../../components/Comments";

describe("Comments", () => {
  test("should render component", async () => {
    const kids = [];
    const { getByTestId } = await render(<Comments kids={kids} />);
    const articles = getByTestId("comments");
    expect(articles).toBeTruthy();
  });
});
