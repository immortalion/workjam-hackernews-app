import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";
import Article from "../../components/Article";

describe("Article", () => {
  let component;
  beforeEach(() => {
    const items = [
      { title: "Hello world!", by: "John Doe", url: "", kids: [] },
    ];
    component = render(<Article {...items} />);
  });

  test("should render component", () => {
    expect(component).toBeTruthy();
  });

  test("should render title", async () => {
    const title = component.getByTestId("article-title");
    expect(title).toBeTruthy();
  });

  test("should render external link", async () => {
    const link = component.getByTestId("article-link");
    expect(link).toBeTruthy();
  });

  test("should render button", async () => {
    const button = component.getByTestId("article-button");
    expect(button).toBeTruthy();
  });
});
