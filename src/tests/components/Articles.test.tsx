import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import Articles from "../../components/Articles";

describe("Articles", () => {
  test("should render component", async () => {
    const items = [
      { title: "Hello world!", by: "John Doe", url: "", kids: [] },
    ];
    const { getByTestId } = await render(<Articles items={items} />);
    const articles = getByTestId("articles");
    expect(articles).toBeTruthy();
  });

  test("should render a list of articles", async () => {
    const items = [
      { title: "Hello world!", by: "John Doe", url: "", kids: [] },
    ];
    const { getByTestId } = render(<Articles items={items} />);

    const article = await getByTestId("article");
    expect(article).toBeTruthy();
  });
});
