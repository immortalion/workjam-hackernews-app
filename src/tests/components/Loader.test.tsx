import React from "react";
import { render } from "@testing-library/react";
import Loader from "../../components/Loader";

describe("Loader", () => {
  test("should render components", () => {
    const { getByTestId } = render(<Loader message="component" />);
    const loader = getByTestId("loader");
    expect(loader).toBeTruthy();
  });

  test("should render the message", () => {
    const message = "component";
    const { getByText } = render(<Loader message={message} />);
    const label = getByText(/Loading/);
    const msg = getByText(/component/);
    expect(label).toBeTruthy();
    expect(msg).toBeTruthy();
  });
});
