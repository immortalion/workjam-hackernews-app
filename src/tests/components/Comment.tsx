import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";
import Comments from "../../components/Comments";

describe("Comments", () => {
  let component;
  beforeEach(() => {
    const items = [
      { title: "Hello world!", by: "John Doe", id: 10010 },
    ];
    component = render(<Comment {...items} />);
  });

  test("should render component", () => {
    expect(component).toBeTruthy();
  });

  test("should render text", async () => {
    const title = component.getByTestId("comment-text");
    expect(title).toBeTruthy();
  });
});
