import React from "react";
import { render } from "@testing-library/react";
import App from "../App";

describe("App", () => {
  test("should render the title", () => {
    const { getByTestId } = render(<App />);
    const header = getByTestId("app-header");
    expect(header).toBeTruthy();
  });

  test("should render articles components", () => {
    const { getByTestId } = render(<App />);
    const articles = getByTestId("articles");
    expect(articles).toBeTruthy();
  });
});
