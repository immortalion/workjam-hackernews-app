export interface Article {
  title: string;
  by: string;
  url: string;
  kids: Array<number>;
}

export interface Payload {
  data: Article | Array<Article>;
}

export interface Kid {
  id: number;
  by: string;
  text: string;
}
