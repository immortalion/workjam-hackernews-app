import React, { SFC, useState, useEffect } from "react";
import styled from "styled-components";

import Articles from "./components/Articles";
import { HACKER_NEWS_API } from "./constants";
import { request } from "./utils";
import { Article as Item, Payload } from "./types";

const Wrapper = styled.div.attrs({
  "data-testid": "app",
})`
  text-align: center;
`;

const Logo = styled.span`
  margin-right: 20px;
`;

const Header = styled.header.attrs({
  "data-testid": "app-header",
})`
  background-color: #1aade3;
  min-height: 10vh;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  font-size: 45px;
  color: #ffff3c;
`;

const Main = styled.div`
  margin: 0 auto;
  max-width: 600px;
`;

const App: SFC = () => {
  const [articles, setArticles] = useState<Array<Item> | []>([]);

  useEffect(() => {
    async function fetch(): Promise<Payload> {
      const topNews = await request(
        `${HACKER_NEWS_API}topstories.json?orderBy=%22$key%22&limitToFirst=10`
      );
      return topNews;
    }

    fetch().then(({ data }) => {
      if (Array.isArray(data)) {
        data.map(async (id) => {
          const item = await request(`${HACKER_NEWS_API}item/${id}.json`);
          setArticles((articles) => [...articles, item.data as Item]);
        });
      }
    });
  }, []);

  const items = Array.from(articles);

  return (
    <Wrapper>
      <Header>
        <Logo>🐱‍💻 </Logo>
        <span>Hacker News</span>
      </Header>
      <Main>
        <Articles items={items} />
      </Main>
    </Wrapper>
  );
};

export default App;
