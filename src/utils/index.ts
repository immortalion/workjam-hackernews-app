import axios from "axios";
import { Payload } from "../types";

export async function request(url: string = ""): Promise<Payload> {
  try {
    return await axios.get(url);
  } catch (err) {
    console.error(err);
    return err;
  }
}
