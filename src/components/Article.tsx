import React, { useState } from "react";
import styled from "styled-components";
import Comments from "./Comments";
import { Article as ArticleProps } from "../types";

const Wrapper = styled.div.attrs({
  "data-testid": "article",
})`
  background-color: #c6ffff;
  border: 2px solid #1eade3;
  text-align: left;
  padding: 20px 10px 10px 10px;
  margin-bottom: 20px;
`;

const Header = styled.header.attrs({
  "data-testid": "article-title",
})`
  font-size: 22px;
  font-weight: bold;
`;

const Read = styled.div.attrs({
  "data-testid": "article-link",
})`
  padding: 10px 0;
`;

const Link = styled.a`
  color: #1aade3;
  text-decoration: none;
`;

const Button = styled.button.attrs({
  "data-testid": "article-button",
})`
  height: 30px;
  margin: 5px 0;
  font-size: 14px;
`;

const Article = ({ title, by, url, kids }: ArticleProps) => {
  const [showComments, setShowComments] = useState(false);

  const onShowClick = () => {
    console.log("onShowClick");
    setShowComments(!showComments);
  };

  return (
    <Wrapper>
      <Header>{title}</Header>
      <small>By {by}</small>
      <Read>
        <Link href={url} target="_blank" rel="noopener noreferrer">
          Read ➡️
        </Link>
      </Read>
      {showComments && <Comments kids={kids} />}
      <Button onClick={onShowClick}>
        {!showComments ? "Show " : "Close "} comments 👉
      </Button>
    </Wrapper>
  );
};

export default Article;
