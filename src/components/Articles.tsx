import React from "react";
import styled from "styled-components";
import Article from "./Article";
import Loader from "./Loader";
import { Article as Item } from "../types";

const Wrapper = styled.div.attrs({
    "data-testid": "articles"
})`
  text-align: left;
  padding: 20px 10px 10px 10px;
`;

interface ArticlesProps {
  items: Array<Item>;
}

const Articles = ({ items }: ArticlesProps) => (
  <Wrapper>
    {items.length > 0 ? (
      items.map(({ title, by, url, kids }: Item) => (
        <Article key={title} kids={kids} title={title} by={by} url={url} />
      ))
    ) : (
      <Loader message="articles" />
    )}
  </Wrapper>
);

export default Articles;
