import React from "react";
import styled from "styled-components";

const Wrapper = styled.div.attrs({
  "data-testid": "loader",
})`
  margin: 20px 0 0 0;
  text-align: center;
`;

interface LoaderProps {
  message: string;
}

const Loader = ({ message }: LoaderProps) => (
  <Wrapper>🧑🏽‍🚒 Loading {message} ...</Wrapper>
);

export default Loader;
