import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { request } from "../utils";
import Loader from "./Loader";
import Comment from "./Comment";
import { HACKER_NEWS_API } from "../constants";
import { Kid } from "../types";

const Wrapper = styled.div.attrs({
  "data-testid": "comments",
})`
  background-color: #dedcd8;
  text-align: left;
  padding: 20px 10px 10px 10px;
  margin-bottom: 20px;
`;

interface CommentsProps {
  kids: Array<number>;
}

const Comments = ({ kids }: CommentsProps) => {
  const [comments, setComments] = useState<Array<Kid> | []>([]);
  useEffect(() => {
    if (Array.isArray(kids)) {
      kids.map(async (kid) => {
        const item = await request(
          `${HACKER_NEWS_API}item/${kid}.json?orderBy=%22$key%22&limitToFirst=20`
        );
        setComments((comments) => [...comments, item.data as any]);
      });
    }
  }, [kids]);

  return (
    <Wrapper>
      {comments.length > 0 ? (
        Array.from(comments).map(({ id, text, by }: Kid) => (
          <Comment key={id} id={id} text={text} by={by} />
        ))
      ) : (
        <Loader message="comments" />
      )}
    </Wrapper>
  );
};

export default Comments;
