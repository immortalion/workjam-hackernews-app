import React from "react";
import styled from "styled-components";
import { Kid as CommentProps } from "../types";

const Wrapper = styled.div.attrs({
  "data-testid": "comment",
})`
  background-color: #ffffff;
  border: 2px solid #a6a5a2;
  text-align: left;
  padding: 10px;
  margin-bottom: 10px;
`;

const Text = styled.p.attrs({
  "data-testid": "comment-text",
})``;

const Comment = ({ id, text, by }: CommentProps) => {
  return (
    <Wrapper>
      <Text dangerouslySetInnerHTML={{ __html: text }} />
      <small>By {by}</small>
    </Wrapper>
  );
};

export default Comment;
