# Workjam News App

1. Develop a web application in ReactJS that is consuming HackerNews REST API to:

* Display the top 10 Stories; and
* Display top 20 comments of those stories; and
* Is responsive design

*Note* Please keep in mind the following three main factors:

Performance
Efficiency
Build and design for extensibility

2. Please provide your test in a public git repository

3. Bonus points, include Unit Tests

4. If you can complete the assessment in typescript with minimum effort that would be great.

Documentation:

HackerNews REST API: https://github.com/HackerNews/API