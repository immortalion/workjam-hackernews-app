# Workjam Hackernews App

## Install

To install dependencies, run:

```
yarn install
```

# Run

To start the web client, run

```
yarn start
```

The page should open at http://localhost:3000

## Testing

To run tests:

```
yarn t
```